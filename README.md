# jquery.mouseRight.js

### 介绍
jQuery的右键插件

### 软件架构
插件实现了右键点击,菜单分组,左键绑定,图标的效果


### 效果

[![效果图](https://z3.ax1x.com/2021/04/03/cnpyPf.png)](https://imgtu.com/i/cnpyPf)

### 安装教程

1.  引入jquery地址
2.  引入插件js和css

```
  <script src="https://cdn.bootcdn.net/ajax/libs/jquery/3.6.0/jquery.min.js"><script>
  <!-- 插件文件,必须放在jquery之后 -->
  <link rel="stylesheet" href="jquery.mouseRight.css">
  <script src="jquery.mouseRight.js"></script>
```

### 使用说明

使用方法很简单,可以看`demo.html`.

使用JQ选择对象,然后调用`mouseRight`即可.

支持的参数:


|  参数   | 类型  | 说明  |
|  ----  | ----  | ---- |
| withLeftBind  | bool | 非必填,是否点击左键也弹出菜单 |
| targetRightClick  | function | 非必填,回调函数,可以接受原始元素 |
| menu  | array | 必填,一个数组,格式参考下面代码 |


```
$(elem).mouseRight({
  withLeftBind: withLeftBind,
  targetRightClick: function (event) {
    console.log('原始元素被点击');
    console.log('原始点击事件', event);
    console.log('原始点击元素', event.currentTarget);
  },
  menu: [
    {
      itemName: '作者:临沂奥宏网络科技有限公司',
      icon: 'layui-icon layui-icon-circle-dot'
    },
    {
      itemName: '你点我啊!',
      callback: function (menuEvent, targetEvent) {
        layer.msg('你点我!!!!')
        console.log('菜单项的事件', menuEvent);
        console.log('原始点击事件', targetEvent);
      }
    },
    {
      type: 'line'
    },
    {
      itemName: '国产原创',
      callback: function (menuEvent, targetEvent) {
        layer.confirm('这是一个国产的原创jq插件,<br>不过原作者是:<br>https://www.jq22.com/jquery-info18299<br>我只是加以改造')
      }
    },
  ]
})
```


### 版权说明

木兰协议2.0

此项目从另一个公开的项目改造而成,原项目地址:
https://www.jq22.com/jquery-info18299

原项目没有指定开源版本,但是公开免费下载.不必担心授权问题.