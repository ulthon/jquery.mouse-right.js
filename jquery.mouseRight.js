; (function ($, window) {
  $.prototype.mouseRight = function (options, callback) {
    var defaults, settings, me, _this;
    me = this;
    defaults = {
      menu: [],
      withLeftBind: false,
      targetRightClick: function () { }
    };
    settings = $.extend({}, defaults, options);
    $(this).off('mousedown contextmenu')
    $(this).mousedown(function (targetEvent) {


      if (3 == targetEvent.originalEvent.which || (settings.withLeftBind && targetEvent.originalEvent.which == 1)) {


        var menuDiv = $('<div></div>');
        menuDiv.attr('class', 'ms-right-wrap');
        for (let i = 0; i < settings.menu.length; i++) {

          if (typeof settings.menu[i].type == "undefined") {
            settings.menu[i].type = 'menu'
          }

          if (settings.menu[i].type == 'menu') {
            var menuItem = $('<li></li>');
            menuItem.addClass('ms-item');
            var menuIcon = $('<i></i>');
            menuItem.attr('data-item', i);
            menuIcon.addClass(settings.menu[i].icon);
            menuIcon.attr('data-item', i);
            menuIcon.appendTo(menuItem);
            menuItem.appendTo(menuDiv);
            menuItem.click(function (itemEvent) {
              if (typeof settings.menu[i].callback == "function") {
                settings.menu[i].callback(itemEvent, targetEvent)
              }

              shadeDiv.remove()
              menuDiv.remove()

            })
            menuIcon.after('&nbsp; ' + settings.menu[i].itemName)

          } else if (settings.menu[i].type == 'line') {
            var menuItem = $('<li></li>');
            menuItem.addClass('ms-line');
            menuItem.appendTo(menuDiv);
          }

        }

        menuDiv.prependTo('body');

        menuDiv.css({
          'display': 'block',
          'top': targetEvent.originalEvent.pageY + 'px',
          'left': targetEvent.originalEvent.pageX + 'px'
        })

        if (targetEvent.originalEvent.pageY + menuDiv.outerHeight() > window.innerHeight) {
          menuDiv.css({
            top: targetEvent.originalEvent.pageY - (menuDiv.outerHeight() - (window.innerHeight - targetEvent.originalEvent.pageY)) - 30 + 'px'
          })
        }
        if (targetEvent.originalEvent.pageX + menuDiv.outerWidth() > window.innerWidth) {
          menuDiv.css({
            left: targetEvent.originalEvent.pageX - (menuDiv.outerWidth() - (window.innerWidth - targetEvent.originalEvent.pageX)) - 30 + 'px'
          })
        }

        menuDiv.on('contextmenu', function (event) {
          return false;
        })

        var shadeDiv = $('<div class="ms-right-shade"></div>')

        shadeDiv.prependTo('body').show()

        shadeDiv.mousedown(function (shadeEvent) {


          menuDiv.remove();
          shadeDiv.remove();
        })
        shadeDiv.on('contextmenu', function (event) {
          return false;
        })

        settings.targetRightClick(targetEvent)
      }

      // return false

    })

    $(this).on('contextmenu', function (event) {

      return false;
    })


    return this
  }
})(jQuery, window)